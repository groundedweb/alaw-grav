---
title: 'Legal Blog'
HeroImage: parallex-inner.jpg
HeroText1: 'A Legal Blog'
HeroText2: 'by Ile Adaramola'
content:
  items: '@self.children'
  order:
    by: date
    dir: desc
  pagination: true
  limit: 5
---

