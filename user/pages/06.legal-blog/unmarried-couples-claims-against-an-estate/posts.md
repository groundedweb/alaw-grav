---
title: 'Unmarried Couples Claims Against an Estate'
published: true
date: '11-08-2016 00:00'
publish_date: '11-08-2016 12:00'
visible: true
---

Recently I received an inquiry from a person whose fiancé died without a will. This person wanted to know what rights she had in the estate of her fiancé. When a person dies without a will the laws of intestacy apply to the distribution of the person's property. Under the North Carolina intestacy laws, the hierarchy of people entitled to a deceased person’s property is surviving spouse, children, parents, and lineal descendants. A fiancé or long-term partner doesn't qualify as an heir to receive real or personal property of a decedent.

As such, if a fiancé or long-term partner has made payments for example on a mortgage, car note, or other valid debts of the decedent, the only recourse available is to file a claim against the estate as a creditor to be reimbursed for those expenses. Once a personal representative opens an estate, he or she must publish notice to all persons, firms, or corporations having claims against the estate of the decedent. Notice is published in a newspaper in the county or area where the decedent lived. Creditors must present their claims in writing and state the amount of the debt within 90 days of the first publication to the personal representative or clerk of court. North Carolina law further provides that the personal representative must send notice to known creditors of the estate.  It is suggested that a creditor present their claim as quickly as possible, generally upon knowledge of the decedent’s death and appointment of a personal representative.

Once a claim has been presented to a personal representative, he or she can either accept the claim as valid or reject the claim. Although this remedy isn't a guaranteed form of reimbursement for an unmarried cohabitant, it does provide a mechanism for a person to be reimbursed as a possible creditor of the estate. 