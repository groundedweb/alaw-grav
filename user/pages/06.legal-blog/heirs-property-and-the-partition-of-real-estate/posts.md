---
title: 'Heirs’ Property and the Partition of Real Estate'
published: true
date: '01-11-2016 00:00'
publish_date: '01-11-2016 12:00'
visible: true
---

Heirs’ property is property that is owned by persons who received the property as a result of a person dying without a will. Each owner has an undivided interest in the property. An undivided interest means that one owner doesn’t have the right to exclude others from the property, exclusively possess the property, and cannot point to a specific piece of the property that he or she owns.

With a large number of owners and competing interests, heirs’ property can be challenging to maintain. Too often, heirs’ property is lost to tax foreclosures. One solution to dealing with heirs’ property is Partition. Partition of real estate is a court process that requires filings with the clerk of court in the estate division.

A partition-in-kind divides the land between owners according to their fractional interest. There are many factors used by the court in deciding whether it will be in the best interest of the heirs to divide the real property.  A partition sale occurs when a Court orders that the entire property is sold and the money from the sale is divided amongst the owners based upon their fractional interest. Courts order partition sales if a partition-in-kind will cause substantial injury to any of the landowners. All of the owners are responsible for all of the court costs associated with the partition sale. Those costs will be deducted from the sale amount. The Court will appoint a Commissioner who will then conduct the sale and ensure that all heirs receive their share of the proceeds.

Partition sales are an intricate and detailed process. Always consult an attorney to ensure that each owner’s interest in the property is properly protected. 