---
title: 'What happens when you fail to fund your Revocable Trust?'
published: true
date: '02-01-2016 00:00'
visible: true
---

Many people will use a revocable trust as their primary estate-planning document but fail to fund the trust. Funding a trust means retitling the assets and accounts that you wish the trust to control and placing those assets in the name of the trust.  Sounds quite simple right? However when assets aren’t retitled in the name of the trust, it defeats the purpose of avoiding probate and/or the need for the court to appoint a conservatorship upon the incapacity of the grantor.

It should be understood that it is not your attorney’s responsibility to fund the trust unless this was stated in the fee agreement.   At Adaramola Law, it is our practice to retitle assets in the name of the trust upon the initiation of the trust by having our clients provide us copies of property tax bills and account statements up front. This allows us to proactively prepare the proper transfer documents on a client’s behalf and ensure that this mistake is avoided.

Related to the mistake of not funding the living trust at the beginning is the issue of making sure that the trust remains funded over time.  This becomes critical whenever you purchase or refinance a home, because most banks will require the property to be in your own name, and not in the name of your trust, in order to complete the financing arrangements.  Once the financing has completed, title on the property can be transferred back to the trust, but this final step is inadvertently forgotten.

When a person forgets to fund a trust, assets not in the name of the trust will be part of the probate estate. As part of a living trust plan, a “Pour Over Will” directs that assets not in trust or inadvertently left out of the trust is transferred into the trust upon the testator death. The court will still probate these assets, however the trust provisions control how and to whom the asset will be transferred.

To ensure that your living trust does exactly what you intend for it to do, always fund the trust and review your assets periodically to ensure that they are still in the name of the trust. 